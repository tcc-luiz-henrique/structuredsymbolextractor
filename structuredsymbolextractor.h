/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef STRUCTUREDSYMBOLEXTRACTOR_H
#define STRUCTUREDSYMBOLEXTRACTOR_H

#include "structuredsymbolextractor_global.h"

#include <QtCore/QObject>

#include <cplusplus/CppDocument.h>
#include <cplusplus/SymbolVisitor.h>
#include <cplusplus/Overview.h>

//#include <cpptools/TypeHierarchyBuilder.h>

#include <projectexplorer/nodesvisitor.h>
#include <projectexplorer/project.h>

using namespace ProjectExplorer;

namespace StructuredSymbolExtractor
{

class Folder;
class File;
class Clazz;
class Function;
class Namespace;
class Attribute;
class SourceVisitor;

class STRUCTUREDSYMBOLEXTRACTORSHARED_EXPORT StructuredSymbolExtractor : public QObject
{
    Q_OBJECT
public:
    static StructuredSymbolExtractor *instance();

Q_SIGNALS:
    void dataStructuresChanged(::StructuredSymbolExtractor::StructuredSymbolExtractor *structuredSymbolExtractor);

public Q_SLOTS:
    void collectHierarchyInformation(Project *project);

    QMultiHash<Clazz *, Attribute *> attributesFromClasses() const;
    QMultiHash<File *, Clazz *> classesFromFiles() const;
    Clazz *findClazzByName(QString clazzName);
    Folder *folderFromClazz(Clazz *clazz);
    QMultiHash<Clazz *, Function *> functionsFromClasses() const;
    Namespace *namespaceFromClass(QString clazzName);

protected:
    StructuredSymbolExtractor(QObject *parent = 0);

private:
    void clearData();

    static StructuredSymbolExtractor *m_instance;

    Clazz *m_currentClassRepresentation;

    SourceVisitor *m_sourceVisitor;

    void printInfo() const;
    void printFolderInfo(Folder *folder) const;
    void printNamespaces() const;
};

} // namespace StructuredSymbolExtractor

#endif // STRUCTUREDSYMBOLEXTRACTOR_H

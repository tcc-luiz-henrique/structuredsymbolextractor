/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef CLAZZ_H
#define CLAZZ_H

#include "structuredsymbolextractor_global.h"

#include <QtCore/QStringList>

namespace StructuredSymbolExtractor
{

class Namespace;

class STRUCTUREDSYMBOLEXTRACTORSHARED_EXPORT Clazz
{
public:
    explicit Clazz(const QStringList &baseClassesNames, const QString &fileName,
                   const QString &name, Namespace *enclosingNamespace);

    QStringList baseClassesNames() const;
    void setBaseClassesNames(const QStringList &baseClassesNames);

    QString fileName() const;
    void setFileName(const QString &fileName);

    QString name() const;
    void setName(const QString &name);

    QString completeName() const;
    void setCompleteName(const QString &completeName);

    Namespace *enclosingNamespace();
    void setEnclosingNamespace(Namespace *enclosingNamespace);

private:
    QStringList m_baseClassesNames;
    QString m_fileName;
    QString m_name;
    QString m_completeName;
    Namespace *m_enclosingNamespace;
};

} // namespace StructuredSymbolExtractor

#endif // CLAZZ_H

/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "attribute.h"

namespace StructuredSymbolExtractor
{

Attribute::Attribute()
{
}

Attribute::Attribute(const QString &name, const QString &typeName, const Visibility &visibility, Clazz *typeClazz) :
    m_name(name), m_typeName(typeName), m_visibility(visibility), m_typeClazz(typeClazz)
{
}

QString Attribute::name() const
{
    return m_name;
}

void Attribute::setName(const QString &name)
{
    m_name = name;
}

QString Attribute::typeName() const
{
    return m_typeName;
}

void Attribute::setTypeName(const QString &typeName)
{
    m_typeName = typeName;
}

Visibility Attribute::visibility() const
{
    return m_visibility;
}

void Attribute::setVisibility(const Visibility &visibility)
{
    m_visibility = visibility;
}

Clazz *Attribute::typeClazz() const
{
    return m_typeClazz;
}

void Attribute::setTypeClazz(Clazz *typeClazz)
{
    m_typeClazz = typeClazz;
}

} // namespace StructuredSymbolExtractor

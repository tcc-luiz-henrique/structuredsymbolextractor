#ifndef CLAZZUTILITIES_H
#define CLAZZUTILITIES_H

#include <QtCore/QString>

#include "structuredsymbolextractor_global.h"

namespace StructuredSymbolExtractor
{

class Clazz;

class STRUCTUREDSYMBOLEXTRACTORSHARED_EXPORT ClazzUtilities
{

public:

    /**
     * This method builds a name that represents the complete name
     * of a given clazz. Hierarchy starts from global namespace till
     * the given namespace, separating them with bars.
     * @brief generateCompleteName
     * @param clazz
     * @return the complete name in formate /EnclosingNamespace/GivenClazz
     */
    static QString generateCompleteName(Clazz *clazz);

private:

    /**
     * Utility class
     * @brief ClazzUtilities
     */
    ClazzUtilities();
};

} // namespace StructuredSymbolExtractor

#endif // CLAZZUTILITIES_H

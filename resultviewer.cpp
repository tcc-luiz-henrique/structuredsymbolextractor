/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "resultviewer.h"

#include "attribute.h"
#include "clazz.h"
#include "elementstorer.h"
#include "file.h"
#include "folder.h"
#include "function.h"
#include "namespace.h"
#include "utilities.h"

#include <QtCore/QDebug>

namespace StructuredSymbolExtractor {

void ResultViewer::printInfo() const
{
    bool printInfoActivated = true;

    ElementStorer *elementStorer = ElementStorer::instance();

    if (elementStorer->startupProject() && printInfoActivated) {
        qDebug() << "######### Running AdiExtractor... #########";
        qDebug() << elementStorer->classesFromFiles().values().size() << "classes found";
        qDebug() << "######### Folders #########";

        foreach (Folder *folder, elementStorer->projectFolders()) {
            printFolderInfo(folder);
        }

        // printNamespaces();
    }
}

void ResultViewer::printFolderInfo(Folder *folder) const
{
    QString *completeFolderName = new QString;
    Utilities::buildCompleteFolderName(folder, completeFolderName);

    ElementStorer *elementStorer = ElementStorer::instance();

    qDebug() << "######### Contents of" << *completeFolderName << "folder #########";
    foreach (File *file, elementStorer->filesFromFolders().values(folder)) {
        qDebug() << file->name();
        foreach (Clazz *classRepresentation, elementStorer->classesFromFiles().values(file)) {
            qDebug() << "   " << classRepresentation->name() << ":" << classRepresentation->baseClassesNames();

            qDebug() << "   " << "Attributes";
            foreach (Attribute *attribute, elementStorer->attributesFromClasses().values(classRepresentation)) {
                qDebug() << "     "
                         << attribute->visibility()
                         << attribute->typeName()
                         << attribute->name();
            }

            qDebug() << "   " << "Functions";
            foreach (Function *function, elementStorer->functionsFromClasses().values(classRepresentation)) {
                qDebug() << "     "
                         << function->visibility()
                         << function->returnType()
                         << function->name()
                         << function->arguments();;
            }
        }
    }

    delete completeFolderName;
}

void ResultViewer::printNamespaces() const
{
    ElementStorer *elementStorer = ElementStorer::instance();

    qDebug() << "######### Namespaces #########";
    foreach (Namespace *namespaceRepresentation, elementStorer->namespaceMultiHash().keys()) {
        qDebug() << namespaceRepresentation->completeName();
    }
}

} // namespace StructuredSymbolExtractor

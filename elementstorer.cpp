/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "elementstorer.h"

#include "clazz.h"
#include "elementconverter.h"
#include "file.h"
#include "folder.h"
#include "namespace.h"

#include <cplusplus/Symbols.h>

namespace StructuredSymbolExtractor {

ElementStorer *ElementStorer::m_instance = 0;

ElementStorer::ElementStorer(QObject *parent) :
    QObject(parent)
{
}

ElementStorer *ElementStorer::instance()
{
    if (!m_instance) {
        m_instance = new ElementStorer;
    }

    return m_instance;
}

bool ElementStorer::storedNamespace(Namespace *namezpace)
{
    foreach (Namespace *storedNamespace, m_namespaceMultiHash.keys()) {
        if (storedNamespace->completeName() == namezpace->completeName()) {
            return true;
        }
    }

    return false;
}

Clazz *ElementStorer::findClazzByName(QString clazzName)
{
    Clazz *clazz = 0;

    foreach (Clazz *currentClazz, m_classesFromFiles.values()) {
        if (currentClazz->name() == clazzName) {
            clazz = currentClazz;
            break;
        }
    }

    if (!clazz) {
        return 0;
    }

    return clazz;
}

Folder *ElementStorer::findFolderByPath(QString folderPath) const
{
    foreach (Folder *folder, m_projectFolders) {
        if (folder->path() == folderPath) {
            return folder;
        }
    }

    return 0;
}

File *ElementStorer::findFileByPath(QString filePath) const
{
    foreach (File *file, m_filesFromFolders.values()) {
        if (file->path() == filePath) {
            return file;
        }
    }

    return 0;
}

Folder *ElementStorer::folderFromClazz(Clazz *clazz)
{
    return m_filesFromFolders.key(m_classesFromFiles.key(clazz));
}

Namespace *ElementStorer::namespaceFromClass(QString clazzName)
{
    return findClazzByName(clazzName)->enclosingNamespace();
}

void ElementStorer::clearData()
{
    m_projectFolders.clear();
    m_classesFromFiles.clear();
    m_functionsFromClasses.clear();
    m_attributesFromClasses.clear();
    m_namespaceMultiHash.clear();
    m_filesFromFolders.clear();
}

void ElementStorer::storeNamespaceBranch(CPlusPlus::Namespace *namezpace) {

    Namespace *namezpaceRepresentation = ElementConverter::fromCPlusPlusNamespace(namezpace);

    // verify if namespace was already stored
    if (!storedNamespace(namezpaceRepresentation)) {

        // creates a parents reference and verify if it was already stored
        // continues visiting parent until visit one that was already stored or until visit GlobalNamespace
        CPlusPlus::Namespace *enclosingNamespace = namezpace->enclosingNamespace();
        if (enclosingNamespace) {
            Namespace *enclosingNamespaceRepresentation = ElementConverter::fromCPlusPlusNamespace(enclosingNamespace);
            //namezpaceRepresentation->setEnclosingNamespace(enclosingNamespaceRepresentation);
            m_namespaceMultiHash.insert(namezpaceRepresentation, enclosingNamespaceRepresentation);
            storeNamespaceBranch(enclosingNamespace);
        } else {
            //namezpaceRepresentation->setEnclosingNamespace(0);
            m_namespaceMultiHash.insert(namezpaceRepresentation, 0);
        }
    }
}


ProjectExplorer::ProjectNode *ElementStorer::startupProject() const
{
    return m_startupProject;
}

void ElementStorer::setStartupProject(ProjectExplorer::ProjectNode*startupProject) {
    m_startupProject = startupProject;
}

QList<Folder *> ElementStorer::projectFolders() const
{
    return m_projectFolders;
}

QMultiHash<Folder *, File *> ElementStorer::filesFromFolders() const
{
    return m_filesFromFolders;
}

QMultiHash<File *, Clazz *> ElementStorer::classesFromFiles() const
{
    return m_classesFromFiles;
}

QMultiHash<Clazz *, Function *> ElementStorer::functionsFromClasses() const
{
    return m_functionsFromClasses;
}

QMultiHash<Namespace *, Namespace *> ElementStorer::namespaceMultiHash() const
{
    return m_namespaceMultiHash;
}

QMultiHash<Clazz *, Attribute *> ElementStorer::attributesFromClasses() const
{
    return m_attributesFromClasses;
}

} // namespace StructuredSymbolExtractor

/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef RESULTVIEWER_H
#define RESULTVIEWER_H

#include "structuredsymbolextractor_global.h"

#include <QtCore/QObject>

namespace StructuredSymbolExtractor {

class Folder;

class STRUCTUREDSYMBOLEXTRACTORSHARED_EXPORT ResultViewer
{

public:
    void printInfo() const;
    void printFolderInfo(Folder *folder) const;
    void printNamespaces() const;

private:
    /**
     * Utility class
     * @brief ResultViewer
     */
    ResultViewer();
};
} // namespace StructuredSymbolExtractor

#endif // RESULTVIEWER_H

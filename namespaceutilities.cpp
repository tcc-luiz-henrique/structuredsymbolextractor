/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "namespaceutilities.h"

#include "namespace.h"

#include <cplusplus/Symbols.h>
#include <cplusplus/Overview.h>

#include <QtCore/QStack>

#include <QtCore/QDebug>
#include <QtCore/QStringList>

namespace StructuredSymbolExtractor
{

CPlusPlus::Overview *NamespaceUtilities::m_overview = new CPlusPlus::Overview;

NamespaceUtilities::NamespaceUtilities()
{
}

QString NamespaceUtilities::generateCompleteName(CPlusPlus::Namespace *namezpace)
{
    QString completeNameBuilder;
    QStack<QString> namespaceNameStack;
    CPlusPlus::Namespace *currentNamespace = namezpace;
    QString currentNamespacePrettyName;

    while (currentNamespace) {
        currentNamespacePrettyName = m_overview->prettyName(currentNamespace->name());
        if (currentNamespacePrettyName.isEmpty()) {
            namespaceNameStack.append(QString::fromLatin1("GlobalNamespace"));
        } else {
            namespaceNameStack.append(currentNamespacePrettyName);
            namespaceNameStack.append(QString::fromLatin1("/"));
        }
        currentNamespace = currentNamespace->enclosingNamespace();
    }

    do {
        completeNameBuilder += namespaceNameStack.pop();
    } while (namespaceNameStack.size() > 0);

    return completeNameBuilder;
}

Namespace * NamespaceUtilities::fromCPlusPlusNamespace(CPlusPlus::Namespace *namezpace)
{
    Namespace *convertedNamespace = new Namespace;

    convertedNamespace->setName(m_overview->prettyName(namezpace->name()));
    convertedNamespace->setCompleteName(generateCompleteName(namezpace));

    return convertedNamespace;
}

} // namespace StructuredSymbolExtractor

/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "file.h"

namespace StructuredSymbolExtractor
{

File::File(const QString &name, const QString &path, const QString &folderName) :
    m_name(name), m_path(path), m_folderName(folderName)
{
}

QString File::name() const
{
    return m_name;
}

void File::setName(const QString &name)
{
    m_name = name;
}

QString File::path() const
{
    return m_path;
}

void File::setPath(const QString &path)
{
    m_path = path;
}

QString File::folderName() const
{
    return m_folderName;
}

void File::setFolderName(const QString &folderName)
{
    m_folderName = folderName;
}

} // namespace StructuredSymbolExtractor

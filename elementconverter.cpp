/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "elementconverter.h"

#include "namespace.h"
#include "utilities.h"

#include <cplusplus/Symbols.h>
#include <cplusplus/Overview.h>

namespace StructuredSymbolExtractor {

Namespace *ElementConverter::fromCPlusPlusNamespace(CPlusPlus::Namespace *namezpace)
{
    Namespace *convertedNamespace = new Namespace;

    CPlusPlus::Overview *overview = new CPlusPlus::Overview;

    convertedNamespace->setName(overview->prettyName(namezpace->name()));
    convertedNamespace->setCompleteName(Utilities::generateCompleteName(namezpace));

    return convertedNamespace;
}

} // namespace StructuredSymbolExtractor

#include "clazzutilities.h"

#include <QtCore/QDebug>
#include <QtCore/QString>
#include <QtCore/QStack>

#include "namespace.h"
#include "clazz.h"

namespace StructuredSymbolExtractor
{

QString ClazzUtilities::generateCompleteName(Clazz *clazz)
{
    if (!clazz) {
        return QString::fromLatin1("");
    }

    return clazz->enclosingNamespace()->completeName() + QString::fromLatin1("/") + clazz->name();
}

} // namespace StructuredSymbolExtractor

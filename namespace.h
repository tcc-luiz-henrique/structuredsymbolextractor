/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef NAMESPACE_H
#define NAMESPACE_H

#include <QtCore/QString>

#include "structuredsymbolextractor_global.h"

namespace StructuredSymbolExtractor
{

class STRUCTUREDSYMBOLEXTRACTORSHARED_EXPORT Namespace
{
public:
    Namespace();
    explicit Namespace(const QString &name);

    QString name() const;
    void setName(const QString &name);

    QString completeName() const;
    void setCompleteName(const QString &completeName);

    Namespace *enclosingNamespace();
    void setEnclosingNamespace(Namespace *enclosingNamespace);

private:
    QString m_name;

    /** Convenience member for obtain a unique name for a namespace.
     * The complete name is a concatenation of the namespace name with names from parents recursively */
    QString m_completeName;

    Namespace *m_enclosingNamespace;
};

} // namespace StructuredSymbolExtractor

#endif // NAMESPACE_H

/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef STRUCTUREDSYMBOLEXTRACTORPLUGIN_H
#define STRUCTUREDSYMBOLEXTRACTORPLUGIN_H

#include "structuredsymbolextractor_global.h"

#include <extensionsystem/iplugin.h>

namespace StructuredSymbolExtractor
{

class StructuredSymbolExtractor;

class STRUCTUREDSYMBOLEXTRACTORSHARED_EXPORT StructuredSymbolExtractorPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.liveblue.StructuredSymbolExtractorPlugin" FILE "StructuredSymbolExtractor.json")

public:
    StructuredSymbolExtractorPlugin();
    ~StructuredSymbolExtractorPlugin();

    static StructuredSymbolExtractorPlugin *instance();
    StructuredSymbolExtractor *extractor() const;
    
    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();
    
private:
    StructuredSymbolExtractor *m_extractor;
    static StructuredSymbolExtractorPlugin *m_instance;
};

} // namespace StructuredSymbolExtractor

#endif // STRUCTUREDSYMBOLEXTRACTORPLUGIN_H

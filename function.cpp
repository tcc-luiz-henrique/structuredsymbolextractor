/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "function.h"

namespace StructuredSymbolExtractor
{

Function::Function(QString name, QString returnType, QList<Function::Argument> arguments, Visibility visibility)
    : m_name(name), m_returnType(returnType), m_arguments(arguments), m_visibility(visibility)
{
}

Function::Function()
{
}

QString Function::name()
{
    return m_name;
}

void Function::setName(QString name)
{
    m_name = name;
}

QString Function::returnType()
{
    return m_returnType;
}

void Function::setReturnType(QString returnType)
{
    m_returnType = returnType;
}

QList<Function::Argument> Function::arguments()
{
    return m_arguments;
}

void Function::setArguments(QList<Function::Argument> arguments)
{
    m_arguments = arguments;
}

Visibility Function::visibility()
{
    return m_visibility;
}

void Function::setVisibility(Visibility visibility)
{
    m_visibility = visibility;
}

} // namespace StructuredSymbolExtractor

/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "namespace.h"

namespace StructuredSymbolExtractor
{

Namespace::Namespace()
{
}

Namespace::Namespace(const QString &name)
    : m_name(name)
{
}

QString Namespace::name() const
{
    return m_name;
}

void Namespace::setName(const QString &name)
{
    m_name = name;
}

QString Namespace::completeName() const
{
    return m_completeName;
}

void Namespace::setCompleteName(const QString &completeName)
{
    m_completeName = completeName;
}

Namespace *Namespace::enclosingNamespace()
{
    return m_enclosingNamespace;
}

void Namespace::setEnclosingNamespace(Namespace *enclosingNamespace)
{
    m_enclosingNamespace = enclosingNamespace;
}

} // namespace StructuredSymbolExtractor

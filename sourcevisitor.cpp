/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "sourcevisitor.h"

#include "attribute.h"
#include "clazz.h"
#include "elementconverter.h"
#include "elementstorer.h"
#include "file.h"
#include "folder.h"
#include "function.h"
#include "utilities.h"

#include <QtCore/QDebug>

#include <cplusplus/Overview.h>
#include <cplusplus/Symbols.h>
#include <projectexplorer/project.h>
#include <projectexplorer/projectnodes.h>

namespace StructuredSymbolExtractor
{

SourceVisitor* SourceVisitor::m_instance = 0;

SourceVisitor::SourceVisitor(QObject *parent) :
    QObject(parent)
{
}

SourceVisitor* SourceVisitor::instance()
{
    if (!m_instance) {
        m_instance = new SourceVisitor;
    }

    return m_instance;
}

void SourceVisitor::visitProjectNode(ProjectExplorer::ProjectNode *projectNode)
{
    visitFolderNode(projectNode);
}

void SourceVisitor::visitFolderNode(ProjectExplorer::FolderNode *folderNode)
{
    ElementStorer *elementStorer = ElementStorer::instance();

    if (folderNode
            && folderNode->nodeType() == ProjectExplorer::VirtualFolderNodeType) {

        Folder *parent = 0;

        if (folderNode->parentFolderNode()) {
            parent = elementStorer->findFolderByPath(folderNode->parentFolderNode()->path());
        }

        Folder *folder = new Folder(folderNode->displayName(), folderNode->path(), parent);
        elementStorer->projectFolders() << folder;

        foreach (ProjectExplorer::FileNode *fileNode, folderNode->fileNodes())
        {
            if (fileNode && !fileNode->isGenerated()) {
                QString fileName = Utilities::getFileName(fileNode->path());
                File *file = new File(fileName, QString(fileNode->path()), folder->name());
                elementStorer->filesFromFolders().insert(folder, file);
            }
        }
    }
}

bool SourceVisitor::visit(CPlusPlus::Class *clazz)
{
    if (!clazz) {
        return false;
    }

    CPlusPlus::Overview overview;

    ElementStorer *elementStorer = ElementStorer::instance();

    QStringList baseClassesNames;
    QString fileName(QString::fromLatin1(clazz->fileName()));
    QString className(overview.prettyName(clazz->name()));

    for (unsigned int i = 0; i < clazz->baseClassCount(); ++i) {
        baseClassesNames << overview.prettyName(clazz->baseClassAt(i)->name());
    }

    CPlusPlus::Namespace *enclosingNamespace = clazz->enclosingNamespace();
    elementStorer->storeNamespaceBranch(enclosingNamespace);

    m_currentClass = clazz;
    m_currentClassRepresentation = new Clazz(baseClassesNames, fileName, className, ElementConverter::fromCPlusPlusNamespace(enclosingNamespace));
    m_currentClassRepresentation->setCompleteName(Utilities::generateCompleteName(m_currentClassRepresentation));

    File *fileFromThisClass = elementStorer->findFileByPath(QString::fromLatin1(clazz->fileName()));

    if (fileFromThisClass) {
        elementStorer->classesFromFiles().insert(fileFromThisClass, m_currentClassRepresentation);
    }

    return true;
}

bool SourceVisitor::visit(CPlusPlus::Declaration *declaration)
{
    CPlusPlus::Overview overview;
    ElementStorer *elementStorer = ElementStorer::instance();

    if (declaration && !declaration->isGenerated() && declaration->type() && declaration->type()->asFunctionType())
    {
        QString functionName = overview.prettyName(declaration->name());
        QString returnType = overview.prettyType(declaration->type()->asFunctionType()->returnType());

        QList<Function::Argument> arguments;
        for (unsigned int i = 0; i < declaration->type()->asFunctionType()->argumentCount(); ++i) {
            QPair<QString, QString> argument;
            argument.first = Utilities::isolateUnqualifiedTypeName(overview.prettyType(declaration->type()->asFunctionType()->argumentAt(i)->asArgument()->type()));
            argument.second = overview.prettyName(declaration->type()->asFunctionType()->argumentAt(i)->asArgument()->name());
            arguments.append(argument);
        }

        Visibility visibility;
        if (declaration->isPublic()) {
            visibility = Public;
        } else if (declaration->isPrivate()) {
            visibility = Private;
        } else {
            visibility = Protected;
        }

        Function *function = new Function(functionName, returnType, arguments, visibility);
         elementStorer->functionsFromClasses().insert(m_currentClassRepresentation, function);

    } else if (declaration->isDeclaration()) {
        QString attributeName = overview.prettyName(declaration->name());

        QString typeName = overview.prettyType(declaration->type());

        if (declaration->type()->asClassType() && declaration->type()->asClassType()->enclosingNamespace()
                && declaration->type()->asClassType()->enclosingNamespace()->name() ) {

            qDebug() << "FOUND NAMESPACE!!!" << overview.prettyName(declaration->type()->asClassType()->enclosingNamespace()->name());
        }

        Visibility visibility;
        if (declaration->isPublic()) {
            visibility = Public;
        } else if (declaration->isPrivate()) {
            visibility = Private;
        } else {
            visibility = Protected;
        }

        Attribute *attribute = new Attribute(attributeName, Utilities::isolateUnqualifiedTypeName(typeName), visibility, 0);
        elementStorer->attributesFromClasses().insert(m_currentClassRepresentation, attribute);
    }

    return true;
}

void SourceVisitor::clear()
{
    m_startupProject = 0;
}

} // namespace StructuredSymbolExtractor

/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef FUNCTION_H
#define FUNCTION_H

#include "visibility.h"
#include "structuredsymbolextractor_global.h"

#include <QtCore/QPair>
#include <QtCore/QList>
#include <QtCore/QString>

namespace StructuredSymbolExtractor
{

class STRUCTUREDSYMBOLEXTRACTORSHARED_EXPORT Function
{
public:
    // First QString is the argument type. Second is argument name.
    typedef QPair<QString, QString> Argument;

    Function(QString name, QString returnType, QList<Argument> arguments, Visibility visibility);
    explicit Function();

    QString name();
    void setName(QString name);

    QString returnType();
    void setReturnType(QString returnType);

    QList<Argument> arguments();
    void setArguments(QList<Argument> arguments);

    Visibility visibility();
    void setVisibility(Visibility visibility);

private:
    QString m_name;
    QString m_returnType;
    QList<Argument> m_arguments;
    Visibility m_visibility;
};

} // namespace StructuredSymbolExtractor

#endif // FUNCTION_H

/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef ELEMENTSTORER_H
#define ELEMENTSTORER_H

#include "structuredsymbolextractor_global.h"

#include <QtCore/QObject>
#include <QtCore/QList>
#include <QtCore/QMultiHash>

#include <projectexplorer/project.h>

namespace CPlusPlus
{
    class Namespace;
}

namespace StructuredSymbolExtractor
{

class Folder;
class File;
class Clazz;
class Function;
class Namespace;
class Attribute;

class STRUCTUREDSYMBOLEXTRACTORSHARED_EXPORT ElementStorer : public QObject
{
    Q_OBJECT
public:
    static ElementStorer *instance();

    bool storedNamespace(Namespace *namezpace);

    // utility functions
    Clazz *findClazzByName(QString clazzName);
    Folder *findFolderByPath(QString folderPath) const;
    File *findFileByPath(QString filePath) const;
    Folder *folderFromClazz(Clazz *clazz);
    Namespace *namespaceFromClass(QString clazzName);

    void clearData();

    void storeNamespaceBranch(CPlusPlus::Namespace *namezpace);

    ProjectExplorer::ProjectNode *startupProject() const;
    void setStartupProject(ProjectExplorer::ProjectNode *startupProject);
    QList<Folder *> projectFolders() const;
    QMultiHash<Folder *, File *> filesFromFolders() const;
    QMultiHash<File *, Clazz *> classesFromFiles() const;
    QMultiHash<Clazz *, Function *> functionsFromClasses() const;
    QMultiHash<Namespace *, Namespace *> namespaceMultiHash() const;
    QMultiHash<Clazz *, Attribute *> attributesFromClasses() const;

signals:
    
public slots:

protected:
    ElementStorer(QObject *parent = 0);

private:
    static ElementStorer* m_instance;

    /**
     * selected project in project explorer
     */
    ProjectExplorer::ProjectNode *m_startupProject;

    /**
     * list all folders from project
     */
    QList<Folder *> m_projectFolders;

    /**
     * relates all file nodes with its folders
     */
    QMultiHash<Folder *, File *> m_filesFromFolders;

    /**
     * relates each class with its files
     */
    QMultiHash<File *, Clazz *> m_classesFromFiles;

    /**
     * relates each function with its class
     */
    QMultiHash<Clazz *, Function *> m_functionsFromClasses;

    /**
     * relates each attribute with its class
     */
    QMultiHash<Clazz *, Attribute *> m_attributesFromClasses;

    /**
     * relates each attribute with its class
     */
    QMultiHash<Namespace *, Namespace *> m_namespaceMultiHash;
    
};
} // namespace StructuredSymbolExtractor

#endif // ELEMENTSTORER_H



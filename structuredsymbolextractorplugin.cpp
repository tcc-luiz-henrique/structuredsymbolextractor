/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "structuredsymbolextractorplugin.h"

#include "structuredsymbolextractor.h"

#include <coreplugin/icore.h>
#include <coreplugin/icontext.h>
#include <coreplugin/coreconstants.h>
#include <coreplugin/actionmanager/command.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/actioncontainer.h>

#include <projectexplorer/session.h>
#include <projectexplorer/project.h>
#include <projectexplorer/projectexplorer.h>

#include <QtCore/QtPlugin>

#include <QMenu>
#include <QAction>
#include <QMessageBox>
#include <QMainWindow>

using namespace ProjectExplorer;

namespace StructuredSymbolExtractor
{

StructuredSymbolExtractorPlugin *StructuredSymbolExtractorPlugin::m_instance = 0;

StructuredSymbolExtractorPlugin::StructuredSymbolExtractorPlugin() :
    m_extractor(StructuredSymbolExtractor::instance())
{
    m_instance = this;
}

StructuredSymbolExtractorPlugin::~StructuredSymbolExtractorPlugin()
{
}

StructuredSymbolExtractorPlugin *StructuredSymbolExtractorPlugin::instance()
{
    return m_instance;
}

StructuredSymbolExtractor *StructuredSymbolExtractorPlugin::extractor() const
{
    return m_extractor;
}

bool StructuredSymbolExtractorPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    Q_UNUSED(arguments)
    Q_UNUSED(errorString)
    SessionManager *sessionManager = (SessionManager*) SessionManager::instance();

    connect(sessionManager, SIGNAL(&startupProjectChanged(ProjectExplorer::Project*)),
            m_extractor, SLOT(&StructuredSymbolExtractor::collectHierarchyInformation(ProjectExplorer::Project*)));

    return true;
}

void StructuredSymbolExtractorPlugin::extensionsInitialized()
{
}

ExtensionSystem::IPlugin::ShutdownFlag StructuredSymbolExtractorPlugin::aboutToShutdown()
{
    return SynchronousShutdown;
}

} // namespace StructuredSymbolExtractor

Q_EXPORT_PLUGIN2(StructuredSymbolExtractor, StructuredSymbolExtractor::StructuredSymbolExtractorPlugin)

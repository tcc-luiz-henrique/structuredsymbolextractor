/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef NAMESPACEUTILITIES_H
#define NAMESPACEUTILITIES_H

#include "structuredsymbolextractor_global.h"

#include <QtCore/QObject>

#include <cplusplus/Symbols.h>
#include <cplusplus/Overview.h>

namespace StructuredSymbolExtractor
{

class Namespace;

class STRUCTUREDSYMBOLEXTRACTORSHARED_EXPORT NamespaceUtilities
{
public:

    /**
     * This method builds a name that represents the complete name
     * of a given namespace. Hierarchy starts from global namespace till
     * the given namespace, separating them with bars.
     * @brief generateCompleteName
     * @param namezpace
     * @return the complete name in formate /ParentNamespace/GivenNamespace
     */
    static QString generateCompleteName(CPlusPlus::Namespace *namezpace);

    static Namespace *fromCPlusPlusNamespace(CPlusPlus::Namespace *namezpace);

private:
    /**
     * Utility class
     * @brief NamespaceUtilities
     */
    NamespaceUtilities();

    static CPlusPlus::Overview *m_overview;
};

} // namespace StructuredSymbolExtractor

#endif // NAMESPACEUTILITIES_H

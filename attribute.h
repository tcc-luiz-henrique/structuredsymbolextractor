/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef ATTRIBUTE_H
#define ATTRIBUTE_H

#include "visibility.h"
#include "structuredsymbolextractor_global.h"

#include <QtCore/QString>

namespace StructuredSymbolExtractor
{

class Clazz;

class STRUCTUREDSYMBOLEXTRACTORSHARED_EXPORT Attribute
{
public:
    explicit Attribute();
    Attribute(const QString &name, const QString &typeName, const Visibility &visibility, Clazz *typeClazz);

    QString name() const;
    void setName(const QString &name);

    QString typeName() const;
    void setTypeName(const QString &typeName);

    Visibility visibility() const;
    void setVisibility(const Visibility &visibility);

    Clazz *typeClazz() const;
    void setTypeClazz(Clazz *typeClazz);

private:
    QString m_name;
    QString m_typeName;
    Visibility m_visibility;
    Clazz *m_typeClazz;
};

} // namespace StructuredSymbolExtractor

#endif // ATTRIBUTE_H

/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef SOURCEVISITOR_H
#define SOURCEVISITOR_H

#include "structuredsymbolextractor_global.h"

#include <QtCore/QObject>

#include <cplusplus/SymbolVisitor.h>
#include <projectexplorer/nodesvisitor.h>

namespace StructuredSymbolExtractor {

class Clazz;

class STRUCTUREDSYMBOLEXTRACTORSHARED_EXPORT SourceVisitor : public QObject, public ProjectExplorer::NodesVisitor, public CPlusPlus::SymbolVisitor
{
    Q_OBJECT
public:
    static SourceVisitor *instance();

    void visitProjectNode(ProjectExplorer::ProjectNode *projectNode);
    void visitFolderNode(ProjectExplorer::FolderNode *folderNode);

    bool visit(CPlusPlus::Class *clazz);
    bool visit(CPlusPlus::Declaration *declaration);

    void clear();

protected:
    SourceVisitor(QObject *parent = 0);

private:
    static SourceVisitor* m_instance;

    CPlusPlus::Class *m_currentClass;
    Clazz *m_currentClassRepresentation;

    ProjectExplorer::ProjectNode *m_startupProject;
    
};
} // namespace StructuredSymbolExtractor

#endif // SOURCEVISITOR_H

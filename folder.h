/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef FOLDER_H
#define FOLDER_H

#include "structuredsymbolextractor_global.h"

#include <QtCore/QString>

namespace StructuredSymbolExtractor
{

class STRUCTUREDSYMBOLEXTRACTORSHARED_EXPORT Folder
{
public:
    explicit Folder(QString name, QString path, Folder *parent);

    QString name();
    void setName(QString name);

    QString path();
    void setPath(QString path);

    Folder *parent();
    void setParent(Folder *parent);

private:
    QString m_name;

    QString m_path;

    Folder *m_parent;
};

} // namespace StructuredSymbolExtractor

#endif // FOLDER_H

/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "structuredsymbolextractor.h"

#include <QtCore/QDebug>
#include <QtCore/QObject>

#include <cplusplus/Icons.h>
#include <cplusplus/Symbols.h>
#include <cplusplus/Literals.h>
#include <cplusplus/CppDocument.h>
#include <cplusplus/TranslationUnit.h>

//#include <cpptools/TypeHierarchyBuilder.h>
//#include <cpptools/ModelManagerInterface.h>

#include <projectexplorer/project.h>
#include <projectexplorer/projectnodes.h>

#include "elementstorer.h"
#include "folder.h"
#include "file.h"
#include "clazz.h"
#include "elementconverter.h"
#include "function.h"
#include "attribute.h"
#include "namespace.h"
#include "sourcevisitor.h"
#include "utilities.h"

namespace StructuredSymbolExtractor
{

StructuredSymbolExtractor *StructuredSymbolExtractor::m_instance = 0;

StructuredSymbolExtractor::StructuredSymbolExtractor(QObject *parent)
    :QObject(parent)
{
    m_sourceVisitor = SourceVisitor::instance();
}

void StructuredSymbolExtractor::collectHierarchyInformation(Project *project)
{
    clearData();
    ElementStorer *elementStorer = ElementStorer::instance();

    if (!project) {
        return;
    }

    elementStorer->setStartupProject(project->rootProjectNode());
    elementStorer->startupProject()->accept(m_sourceVisitor);

    qDebug() << "starting to read files...";

    // read all files for extract theier classes
    foreach (Folder *folder, elementStorer->projectFolders()) {
        qDebug() << folder->name();
        foreach (File *fileRepresentation, elementStorer->filesFromFolders().values(folder)) {

            qDebug() << fileRepresentation->name();
            QString filePath = fileRepresentation->path();
            if (!filePath.endsWith(QLatin1String(".h"))) {
                continue;
            }

            QFile file(filePath);
            if (!file.open(QIODevice::ReadOnly|QIODevice::Text)) {
                qDebug() << "could not open" << filePath;
                return;
            }

            CPlusPlus::Snapshot *snapshot = new CPlusPlus::Snapshot;
            CPlusPlus::Document::Ptr doc = snapshot->documentFromSource(file.readAll(), filePath);
            if (doc.isNull()) {
                qDebug() << "null doc";
                return;
            }

            doc->check(CPlusPlus::Document::FastCheck);
            if (!doc->globalNamespace()) {
                qDebug() << "null globalNamespace";
                continue;
            }

            m_currentClassRepresentation = 0;
            m_sourceVisitor->accept((CPlusPlus::Symbol *) doc->globalNamespace());

        }
    }

    printInfo();

    emit dataStructuresChanged(m_instance);
}

QMultiHash<File *, Clazz *> StructuredSymbolExtractor::classesFromFiles() const
{
    return ElementStorer::instance()->classesFromFiles();
}

Clazz *StructuredSymbolExtractor::findClazzByName(QString clazzName)
{
    return ElementStorer::instance()->findClazzByName(clazzName);
}

QMultiHash<Clazz *, Attribute *> StructuredSymbolExtractor::attributesFromClasses() const
{
    return ElementStorer::instance()->attributesFromClasses();
}

Folder *StructuredSymbolExtractor::folderFromClazz(Clazz *clazz)
{
    return ElementStorer::instance()->folderFromClazz(clazz);
}

QMultiHash<Clazz *, Function *> StructuredSymbolExtractor::functionsFromClasses() const
{
    return ElementStorer::instance()->functionsFromClasses();
}

Namespace *StructuredSymbolExtractor::namespaceFromClass(QString clazzName)
{
    return ElementStorer::instance()->namespaceFromClass(clazzName);
}

void StructuredSymbolExtractor::clearData()
{
    // TODO when this class has an ElementStorer, call clearData from ElementStorer
    ElementStorer *elementStorer = ElementStorer::instance();
    elementStorer->clearData();
}


void StructuredSymbolExtractor::printInfo() const
{
    bool printInfoActivated = true;

    ElementStorer *elementStorer = ElementStorer::instance();

    if (elementStorer->startupProject() && printInfoActivated) {
        qDebug() << "######### Running AdiExtractor... #########";
        qDebug() << elementStorer->classesFromFiles().values().size() << "classes found";
        qDebug() << "######### Folders #########";

        foreach (Folder *folder, elementStorer->projectFolders()) {
            printFolderInfo(folder);
        }

        //        printNamespaces();
    }
}

void StructuredSymbolExtractor::printFolderInfo(Folder *folder) const
{
    ElementStorer *elementStorer = ElementStorer::instance();

    QString *completeFolderName = new QString;
    Utilities::buildCompleteFolderName(folder, completeFolderName);

    qDebug() << "######### Contents of" << *completeFolderName << "folder #########";
    foreach (File *file, elementStorer->filesFromFolders().values(folder)) {
        qDebug() << file->name();
        foreach (Clazz *classRepresentation, elementStorer->classesFromFiles().values(file)) {
            qDebug() << "   " << classRepresentation->name() << ":" << classRepresentation->baseClassesNames();

            qDebug() << "   " << "Attributes";
            foreach (Attribute *attribute, elementStorer->attributesFromClasses().values(classRepresentation)) {
                qDebug() << "     "
                         << attribute->visibility()
                         << attribute->typeName()
                         << attribute->name();
            }

            qDebug() << "   " << "Functions";
            foreach (Function *function, elementStorer->functionsFromClasses().values(classRepresentation)) {
                qDebug() << "     "
                         << function->visibility()
                         << function->returnType()
                         << function->name()
                         << function->arguments();;
            }
        }
    }

    delete completeFolderName;
}

void StructuredSymbolExtractor::printNamespaces() const
{
    ElementStorer *elementStorer = ElementStorer::instance();

    qDebug() << "######### Namespaces #########";
    foreach (Namespace *namespaceRepresentation, elementStorer->namespaceMultiHash().keys()) {
        qDebug() << namespaceRepresentation->completeName();
    }
}

StructuredSymbolExtractor *StructuredSymbolExtractor::instance()
{
    if (!m_instance)
        m_instance = new StructuredSymbolExtractor;
    return m_instance;
}

} // namespace StructuredSymbolExtractor

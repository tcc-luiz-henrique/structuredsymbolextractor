/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "clazz.h"

namespace StructuredSymbolExtractor
{

Clazz::Clazz(const QStringList &baseClassesNames, const QString &fileName,
             const QString &name, Namespace *enclosingNamespace) :
    m_baseClassesNames(baseClassesNames), m_fileName(fileName), m_name(name), m_enclosingNamespace(enclosingNamespace)
{
}

QString Clazz::name() const
{
    return m_name;
}

void Clazz::setName(const QString &name)
{
    m_name = name;
}

QString Clazz::completeName() const
{
    return m_completeName;
}

void Clazz::setCompleteName(const QString &completeName)
{
    m_completeName = completeName;
}

Namespace *Clazz::enclosingNamespace()
{
    return m_enclosingNamespace;
}

void Clazz::setEnclosingNamespace(Namespace *enclosingNamespace)
{
    m_enclosingNamespace = enclosingNamespace;
}

QStringList Clazz::baseClassesNames() const
{
    return m_baseClassesNames;
}

void Clazz::setBaseClassesNames(const QStringList &baseClassesNames)
{
    m_baseClassesNames = baseClassesNames;
}

QString Clazz::fileName() const
{
    return m_fileName;
}

void Clazz::setFileName(const QString &fileName)
{
    m_fileName = fileName;
}

} // namespace StructuredSymbolExtractor

/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "folder.h"

namespace StructuredSymbolExtractor
{

Folder::Folder(QString name, QString path, Folder *parent) :
    m_name(name), m_path(path), m_parent(parent)
{
}

QString Folder::name()
{
    return m_name;
}

void Folder::setName(QString name)
{
    m_name = name;
}

QString Folder::path()
{
    return m_path;
}

void Folder::setPath(QString path)
{
    m_path = path;
}

Folder *Folder::parent()
{
    return m_parent;
}

void Folder::setParent(Folder *parent)
{
    m_parent = parent;
}

} // namespace StructuredSymbolExtractor

/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "utilities.h"

#include "clazz.h"
#include "folder.h"
#include "namespace.h"

#include <QtCore/QString>
#include <QtCore/QStack>

#include <cplusplus/Symbols.h>
#include <cplusplus/Overview.h>

namespace StructuredSymbolExtractor
{

QString Utilities::generateCompleteName(Clazz *clazz)
{
    if (!clazz) {
        return QString::fromLatin1("");
    }

    return clazz->enclosingNamespace()->completeName() + QString::fromLatin1("/") + clazz->name();
}

QString Utilities::generateCompleteName(CPlusPlus::Namespace *namezpace)
{
    QString completeNameBuilder;
    QStack<QString> namespaceNameStack;
    CPlusPlus::Namespace *currentNamespace = namezpace;
    QString currentNamespacePrettyName;

    CPlusPlus::Overview *overview = new CPlusPlus::Overview;

    while (currentNamespace) {
        currentNamespacePrettyName = overview->prettyName(currentNamespace->name());
        if (currentNamespacePrettyName.isEmpty()) {
            namespaceNameStack.append(QString::fromLatin1("GlobalNamespace"));
        } else {
            namespaceNameStack.append(currentNamespacePrettyName);
            namespaceNameStack.append(QString::fromLatin1("/"));
        }
        currentNamespace = currentNamespace->enclosingNamespace();
    }

    do {
        completeNameBuilder += namespaceNameStack.pop();
    } while (namespaceNameStack.size() > 0);

    return completeNameBuilder;
}

QString Utilities::isolateUnqualifiedTypeName(QString rawTypeName) {

    if (rawTypeName == QString::fromLatin1("")) {
        return rawTypeName;
    }

    rawTypeName.remove(QString::fromLatin1("*"));

//    rawTypeName.remove(' ');

    int indexOf = 0;

    while ((indexOf = rawTypeName.indexOf(QString::fromLatin1("::"))) != -1) {
        rawTypeName = rawTypeName.right(rawTypeName.length() - indexOf - 2);
    }

    return rawTypeName;
}

QString Utilities::getFileName(QString filePath)
{
    int lastForwardSlash = -1;
    int size = filePath.size();

    // find last slash
    for (int i = 0; i < size; ++i) {
        if (filePath.at(i) == QChar::fromLatin1('/')) {
            lastForwardSlash = i;
        }
    }

    QString fileName;
    if (lastForwardSlash > -1) {
        fileName = filePath.right(size - lastForwardSlash - 1);
    }

    return fileName;
}

void Utilities::buildCompleteFolderName(Folder *folder, QString *completeName)
{
    completeName->prepend(folder->name());
    completeName->prepend(QString::fromLatin1("/"));

    if (folder->parent()) {
        buildCompleteFolderName(folder->parent(), completeName);
    }
}


} // namespace StuctureSymbolExtractor
